package src.app;

import src.DoubleLinkedList.DoubleLinkedList;

public class App {
	public static void main(String[] args) {
	DoubleLinkedList<String> dll = new DoubleLinkedList<String>();
	
	
	dll.addBegin("jalil");
	
	dll.addBegin("luis");
	
	dll.addBegin("f");
	
	dll.addBegin("lol");
	
	dll.addBegin("alex");
	
	dll.addEnd("e");
	
	dll.remove("e");
	
	dll.replace("alex","no alex");
	
	for (String name : dll) {
	System.out.print(name+" ");
	}
	System.out.println();
	
   System.out.println("Direccion de memoria de no alex : " + dll.searchDoble("no alex"));
	
	dll.addEnd("fin");
	
	dll.addBegin("first");
	
	System.out.println("Primer valor: "+dll.getFirst().getValue());
	
	System.out.println( "Ultimo valor: "+dll.getLast().getValue());
	
	dll.removeFirst();
	
	dll.removeLast();
	
	System.out.println("Esta vacio? :"+dll.isEmpty());
	
	System.out.println("Index of lol: "+ dll.indexOf("lol"));
	
	System.out.println( "Tamaño :"+dll.size());
	
	dll.addAfter("lol", "jojo");
	
	dll.addBefore("lol", "j");
	
	dll.removeBefore("f");
	
    dll.removeAfter("f");
	
	//dll.clear();

    System.out.println("------------------Lista pa'lante -----------------");
	dll.printPalante();
	System.out.println("------------------Lista pa'tras------------------");
	dll.printPatras();
	
	}
	
}
